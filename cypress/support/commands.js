// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('dragAndDrop', (fromContainerID, toContainerID, cardClass) => {
    const dataTransfer = new DataTransfer();
    cy.get(`#${fromContainerID} > .${cardClass}`).first().should('be.visible', { timeout: 2000 });
    cy.get(`#${toContainerID}`).should("be.visible", {timeout: 2000});
    
    cy.get(`#${fromContainerID} > .${cardClass}`).first().trigger("dragstart", {
        dataTransfer
    })
    cy.get(`#${toContainerID}`).trigger("drop", {
        dataTransfer
    })
})