import { defineStore } from "pinia";

export const usePatientStore = defineStore({
  id: "patients",
  state: () => ({
    patients: [],
  }),
  actions: {
    setPatients(newPatientList) {
      patients = newPatientList;
    },
  },
});
