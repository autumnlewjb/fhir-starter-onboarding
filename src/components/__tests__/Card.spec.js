import { mount } from "@vue/test-utils";
import { afterEach, beforeAll, describe, expect, test } from "vitest";
import PatientCard from "../PatientCard.vue";

describe("patient card", () => {
    let wrapper;
    afterEach(() => {
        wrapper.unmount();
    })
    
    test("card with name only", () => {
        wrapper = mount(PatientCard, {props: {item: {name: "Lee Ah Kau"}}});
        expect(wrapper.findAll("p").length).toBe(1);
        expect(wrapper.find("p").attributes('id')).toMatch("card-name");
        expect(wrapper.find("p").text()).toMatch(/^Name: .*$/);
    });
    test("card with all fields except name", () => {
        wrapper = mount(PatientCard, {props: {item: {gender: "male", birthDate: "20-10-2001", address: "This is a random address"}}})
        expect(wrapper.findAll("p").length).toBe(4);
        expect(wrapper.find("#card-name").exists()).toBeTruthy();
        expect(wrapper.find("#card-name").text()).toMatch(/^Name: unknown$/);
        expect(wrapper.find("#card-gender").text()).toMatch(/^Gender: (?:male|female)$/);
        expect(wrapper.find("#card-birthdate").text()).toMatch(/^Birthdate: \d{2}-\d{2}-\d{4}$/);
        expect(wrapper.find("#card-address").text()).toMatch(/^Address: .*$/);
    })
})